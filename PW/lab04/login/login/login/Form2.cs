﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WIzytowka
{
    public partial class Form2 : Form
    {
        string login1 = "Ja";
        string haslo1 = "12"; 

        string login2 = "DobreProgramowanie";
        string haslo2 = "12fajne21";

        public string login,haslo;

        private void key_q(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'q') Close();
        }

        private void wiersz1_ML(object sender, EventArgs e)
        {
            wiersz1.ForeColor = Color.Black;
        }

        private void wiersz1_MH(object sender, EventArgs e)
        {
            wiersz1.ForeColor = Color.Red;
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            if(checkPases())
            {
                wiersz1.Text = "Witaj " + login + "! ";
            }
            else
            {
                wiersz1.Text = "Nie zalogowano";
            }
        }

        private Boolean checkPases()
        {
            if(login == login1 && haslo == haslo1)
            {
                return true;
            }
            else if(login == login2 && haslo == haslo2)
            {
                return true;
            }
            return false;
        }
    }
}