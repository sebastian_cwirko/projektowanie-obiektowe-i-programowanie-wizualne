﻿namespace WIzytowka
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wiersz1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // wiersz1
            // 
            this.wiersz1.AutoSize = true;
            this.wiersz1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wiersz1.Location = new System.Drawing.Point(157, 54);
            this.wiersz1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.wiersz1.Name = "wiersz1";
            this.wiersz1.Size = new System.Drawing.Size(298, 23);
            this.wiersz1.TabIndex = 0;
            this.wiersz1.Text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            this.wiersz1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.wiersz1.MouseLeave += new System.EventHandler(this.wiersz1_ML);
            this.wiersz1.MouseHover += new System.EventHandler(this.wiersz1_MH);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.wiersz1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.key_q);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label wiersz1;
    }
}