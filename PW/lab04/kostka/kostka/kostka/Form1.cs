﻿using System;
using System.Windows.Forms;

namespace kostka
{
    
    public partial class Form1 : Form
    {
        private const int MaxValue = 6;

        public Form1()
        {
            InitializeComponent();
            this.Text = "Rzut kostka k6";
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            randomek();
        }

        private void randomek()
        {
            Random rnd = new Random();
            wynik.Text = rnd.Next(1, MaxValue).ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void doSchowka()
        {
            Clipboard.SetText(wynik.Text);
        }

        private void wynik_DoubleClick(object sender, EventArgs e)
        {
            doSchowka();
        }

       

        private void wynik_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            randomek();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.R)
            {
                randomek();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
    
}
