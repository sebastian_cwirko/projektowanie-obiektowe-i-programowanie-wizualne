﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit_tests
{
    public class Silnia
    {
        int n;

        public Silnia(int n)
        {
            this.n = n;
        }

        public long oblicz()
        {
            long silniaWynik =1;
            for(int i = this.n;i>1;i--)
            {
                silniaWynik *= i;
            }
            return silniaWynik;
        }
    }
}
