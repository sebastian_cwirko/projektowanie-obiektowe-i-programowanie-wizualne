﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unit_tests;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Silnia_4()
        {
            Silnia silnia1 = new Silnia(4);
            Assert.AreEqual(silnia1.oblicz(), 24);
        }
        [TestMethod]
        public void Silnia_10()
        {
            Silnia silnia2 = new Silnia(10);
            Assert.AreEqual(silnia2.oblicz(), 3628800);
        }
    }
}
