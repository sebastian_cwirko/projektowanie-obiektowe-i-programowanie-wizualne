﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diagram_w_Konsoli
{
    class Program
    {
        static void Main(string[] args)
        {
            L1 l1 = new L1();
            L2 l2 = new L2();

            Console.WriteLine("Przed:");

            Console.WriteLine("\tL1:\n\ta: " + l1.getA() + "\tb: " + l1.getB() + "\tc: " + l1.getC()+"\n");
            Console.WriteLine("\tL2:\n\ta: " + l2.getA() + "\tb: " + l2.getB() + "\tc: " + l2.getC() + "\n");

            l1.setA(4);
            l1.setB(2);
            l1.setC(0);

            l2.setA(5);
            l2.setB(0);
            l2.setC(4);

            Console.WriteLine("Po:");

            Console.WriteLine("\tL1:\n\ta: "+ l1.getA() + "\tb: "+ l1.getB()+"\tc: "+ l1.getC() + "\n");
            Console.WriteLine("\tL2:\n\ta: " + l2.getA() + "\tb: "+ l2.getB()+"\tc: "+ l2.getC() + "\n");

            Console.WriteLine("\nPress any key to close");
            Console.ReadKey();
        }
    }
}
